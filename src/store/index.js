import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import taskReducer from "./modules/Tasks/reducer";

const reducers = combineReducers({
  tasks: taskReducer,
});

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
