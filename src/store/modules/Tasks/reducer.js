import { ADD_TASK } from "./actionTypes";

const taskReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_TASK:
      return [...state, action.task];

    default:
      return state;
  }
};

export default taskReducer;
